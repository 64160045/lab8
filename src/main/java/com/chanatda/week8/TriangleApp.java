package com.chanatda.week8;

public class TriangleApp {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(5, 5, 6);
        triangle1.printTriangleArea();
        triangle1.printTrianglePerimeter();
    }
}
