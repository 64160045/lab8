package com.chanatda.week8;

import java.math.*;

public class Triangle {
    // Attributes
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) { // constructor
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public double printTriangleArea() {
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        System.out.println("Calculate area" + " = " + area);
        return area;
    }

    public double printTrianglePerimeter() {
        double perimeter = a + b + c;
        System.out.println("Calculate perimeter" + " = " + perimeter);
        return perimeter;
    }
}
