package com.chanatda.week8;

public class RectangleApp {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        rect1.printRectangleArea();
        Rectangle rect2 = new Rectangle(5, 3);
        rect2.printRectangleArea();
        rect1.printRectanglePerimeter();
        rect2.printRectanglePerimeter();

    }

}
