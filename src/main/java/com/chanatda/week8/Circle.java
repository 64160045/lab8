package com.chanatda.week8;

public class Circle {
    private double radian;

    public Circle(int radian) {
        this.radian = radian;

    }

    public int printCircleRadian() {
        double area = 3.14 * (radian * radian);
        System.out.println(area);
        return (int) area;
    }

    public int printCirclePerimeter() {
        double perimeter = (2 * 3.14) * radian;
        System.out.println(perimeter);
        return (int) perimeter;
    }
}
