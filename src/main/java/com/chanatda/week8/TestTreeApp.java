package com.chanatda.week8;

public class TestTreeApp {
    public static void main(String[] args) {
        TestTree tree1 = new TestTree("tree1", 5, 10);
        TestTree tree2 = new TestTree("tree2", 5, 11);
        tree1.print();
        tree2.print();
    }
}
